<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $fillable = ['name_pt', 'name_en', 'description_pt', 'description_en'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'menu';
    
    public function menuContent()
    {
        return $this->hasMany(MenuContent::class);
    }
}
