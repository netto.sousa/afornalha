<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods = Item::get();
        $categories = Category::get();
        return view('backoffice.pages.foods')
                ->with('foods', $foods)
                ->with('categories', $categories)
                ->with('title', 'foods');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Item::create([
            'name_en' => $request->name_en,
            'name_pt' => $request->name_pt,
            'description_pt' => $request->description_pt,
            'description_en' => $request->description_en,
            'price' => $request->price,
            'category_id' => $request->category_id,
        ]);

        return redirect()->back()->with('success', 'Comida criada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $language, $id)
    {   
        Item::where('id', $id)->update([
            'name_en' => $request->name_en,
            'name_pt' => $request->name_pt,
            'description_pt' => $request->description_pt,
            'description_en' => $request->description_en,
            'price' => $request->price,
            'category_id' => $request->category_id,
        ]);

        return json_encode(['success' => 'Comida atualizada com sucesso!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        Item::destroy($id);
        return redirect()->back()->with('success', 'Comida removida com sucesso!');
    }
}
