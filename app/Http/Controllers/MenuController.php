<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\MenuContent;
use App\Item;
use App\Category;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::get();
        $categories = Category::get();
        $items = Item::get();

        return view('backoffice.pages.menus')
                ->with('menus', $menus)
                ->with('categories', $categories)
                ->with('items', $items)
                ->with('title', 'Menus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Menu::create([
            'name_en' => $request->name_en,
            'name_pt' => $request->name_pt,
        ]);

        return redirect()->back()->with('success', 'Menu created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $menu = Menu::find($id);
        $content = view('backoffice.pages.menu-content-list', compact('menu'))
            ->with('menus', $menu)
            ->render();
        return response()->json([
            'success' => true,
            'content' => $content
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::destroy($id);
        return redirect()->back()->with('success', 'Menu deleted successfully.');
    }

    public function menuContentDestroy($lang, $id)
    {
        MenuContent::destroy($id);
        return redirect()->back()->with('success', 'Menu content deleted successfully.');
    }
}
