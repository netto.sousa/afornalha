<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class MailTreatController extends Controller
{
    public function sendContactForm(Request $request){
        Mail::to('geraldonetto@7graus.com')->send(new ContactMail($request->all()));
        dd($request->all());
    }
}
