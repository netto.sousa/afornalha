<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($language){
        App::setLocale('en');
        return view('index');
    }
}
