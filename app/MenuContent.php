<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuContent extends Model
{
    use HasFactory;
    protected $fillable = ['menu_id', 'item_id'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'menu_content';

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
