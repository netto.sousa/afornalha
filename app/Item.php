<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $fillable = ['name_pt', 'name_en', 'description_pt', 'description_en', 'price', 'category_id'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'items';
    public function category()
    {
        return $this->belongsTo(Category::class)->orderBy('id');
    }
}
