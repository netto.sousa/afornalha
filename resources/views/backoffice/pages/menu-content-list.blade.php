<table class="datatable-edit">
    <thead>
        <tr>
            <th>Categoria</th>
            <th>Comida</th>
            <th>Preço</th>
            <th style="text-align: right;">Ações</th>
        </tr>
    </thead>
    <tbody class="table datatable">
        @if($menu->menuContent->count() > 0)
        @foreach($menu->menuContent as $menuContent)
        <tr>
            <td>{{$menuContent->item->category->name_pt}}</td>
            <td>{{$menuContent->item->name_pt}}</td>
            <td>{{$menuContent->item->price}}</td>
            <td style="text-align: right;">
                <form action="{{ route('menusContent.destroy', ['language' => app()->getLocale(), 'id' => $menu->id]) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-fill btn-sm"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="5" class="text-center">No menus found</td>
        </tr>
        @endif
    </tbody>
</table>

@section('js')
    <script> 
    $(document).ready(function() {
       
    });


    </script>
@stop