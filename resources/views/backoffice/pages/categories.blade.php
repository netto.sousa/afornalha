@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Categorias</h1>
@stop

@section('plugins.Datatables', true)

@section('content')
    <div class="main">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Criar nova Categoria</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('menus.store', app()->getLocale()) }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Português)</label>
                                            <input type="text" class="form-control" name="name_pt" placeholder="Nome do menu em Português">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Inglês)</label>
                                            <input type="text" class="form-control" name="name_en" placeholder="Nome do menu em Inglês">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info btn-fill pull-right">Criar</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listagem de Categorias criadas</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Actions
                                        </th>
                                    </thead>
                                    <tbody>
                                        @if($categories->count() > 0)
                                        @foreach($categories as $category)
                                        <tr>
                                            <td>
                                                {{$category->id}}
                                            </td>
                                            <td>
                                                <b>Português:</b> {{$category->name_pt}} <br> <b>Inglês:</b> {{$category->name_en}}
                                            </td>
                                            <td class="text-primary">
                                                <a href="{{ route('comidas.edit', [app()->getLocale(), $category->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editCategoryModal" data-categoria_id="{{ $category->id }}" data-name_pt="{{ $category->name_pt }}" data-name_en="{{ $category->name_en }}">Editar</a>
                                                <form action="{{ route('categorias.destroy', ['language' => app()->getLocale(), 'categoria' => $category->id]) }}" method="POST" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-fill btn-sm"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="5" class="text-center">No categories found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editCategoryModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editCategoryModalLabel">Editar Categoria</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" method="PUT">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome (Português)</label>
                                    <input type="text" class="form-control" name="name_pt" placeholder="Nome do menu em Português">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome (Inglês)</label>
                                    <input type="text" class="form-control" name="name_en" placeholder="Nome do menu em Inglês">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary save-row">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    <script> 

    $('.select2').select2({
        placeholder: "Selecione um item",
    });
    $(document).ready(function() {
        $('#editCategoryModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var categoria_id = button.data('categoria_id'); // Extract info from data-* attributes
            var modal = $(this);
            modal.find('.modal-body form').attr('action', '/{{ app()->getLocale() }}/backoffice/categorias/' + categoria_id);
            modal.find('.modal-body form input[name="_method"]').val('PUT');
            modal.find('#editCategoryModalLabel').text(`A Editar - ${button.data('name_pt')}`);
            modal.find('.modal-body form input[name="name_pt"]').val(button.data('name_pt'));
            modal.find('.modal-body form input[name="name_en"]').val(button.data('name_en'));
        });

        $('.save-row').click(function(e) {
                e.preventDefault();
                var url = $('#editCategoryModal').find('.modal-body form').attr('action');
                var method = $('#editCategoryModal').find('.modal-body form input[name="_method"]').val();
                console.log(url, method);
                $.ajax({
                    url: url,
                    type: method,
                    data: $('#editCategoryModal .modal-body form').serialize(),
                    success: function(result) {
                        $('#editCategoryModal').modal('hide');
                        location.reload();
                    }
                });
            });

        $('.datatable').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtrado de _MAX_ registros no total)",
                "search": "Pesquisar",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Próximo"
                }
            },
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                { "orderable": false, "targets": 2 }
            ]
        });
    });
    </script>
@stop