@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Menus</h1>
@stop

@section('plugins.Datatables', true)

@section('content')
    <div class="main">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Criar novo Menu</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('menus.store', app()->getLocale()) }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Português)</label>
                                            <input type="text" class="form-control" name="name_pt" placeholder="Nome do menu em Português">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Inglês)</label>
                                            <input type="text" class="form-control" name="name_en" placeholder="Nome do menu em Inglês">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info btn-fill pull-right">Criar</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listagem de Menus criados</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Actions
                                        </th>
                                    </thead>
                                    <tbody>
                                        @if($menus->count() > 0)
                                            @foreach($menus as $menu)
                                            <tr>
                                                <td>
                                                    {{$menu->id}}
                                                </td>
                                                <td>
                                                    {{$menu->name_pt}}
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-fill btn-md" data-toggle="modal" data-target="#editModal" data-menu_id="{{ $menu->id }}" data-menu_name="{{ $menu->name_pt }}"><i class="fa fa-edit"></i></button>
                                                    <form action="{{ route('menus.destroy', ['language' => app()->getLocale(), 'menu' => $menu->id]) }}" method="POST" class="d-inline">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-fill btn-md"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5" class="text-center">No menus found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Edit -->
            <div class="modal modal-fullscreen fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editModalLabel">Editar Menu</h5>
                            <button type="button" onClick="addMenuContent()" class="add-menu-item form-control"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Português)</label>
                                            <input type="text" class="form-control" name="name_pt" placeholder="Nome do menu em Português">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Inglês)</label>
                                            <input type="text" class="form-control" name="name_en" placeholder="Nome do menu em Inglês">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="button" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Edit -->
            <div class="modal modal-fullscreen fade" id="addContentModal" tabindex="-1" role="dialog" aria-labelledby="addContentModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addContentModalLabel">Adicionar Item ao Menu</h5>
                        </div>
                        <div class="modal-body">
                            <form>
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Comidas </label>
                                            <select class="form-control select2" style="width: 100%!important;" name="item_id">
                                                @foreach($items as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name_pt }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
    <style>
    table.datatable-edit{
        width: 100% !important;
    }
    .modal.modal-fullscreen .modal-dialog,
    .modal.modal-fullscreen .modal-content {
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    }
    .modal.modal-fullscreen .modal-dialog {
    margin: 0;
    width: 100%!important;
    max-width: 100%!important;
    animation-duration:0.6s;
    }
    .modal.modal-fullscreen .modal-content {
    border: none;
    -moz-border-radius: 0;
    border-radius: 0;
    -webkit-box-shadow: inherit;
    -moz-box-shadow: inherit;
    -o-box-shadow: inherit;
    box-shadow: inherit;
    }
    .modal.modal-fullscreen.force-fullscreen .modal-body {
    padding: 0;
    }
    .modal.modal-fullscreen.force-fullscreen .modal-header,
    .modal.modal-fullscreen.force-fullscreen .modal-footer {
    left: 0;
    position: absolute;
    right: 0;
    }
    .modal.modal-fullscreen.force-fullscreen .modal-header {
    top: 0;
    }
    .modal.modal-fullscreen.force-fullscreen .modal-footer {
    bottom: 0;
    }

    .add-menu-item{
        margin-right: 1rem;
        max-width: 50px;
        float: right;
        position: absolute;
        right: 0px;
    }


    </style>
@stop

@section('js')
    <script> 

    window.addMenuContent = function(){
        $('#editModal').modal('hide');
        $('#addContentModal').modal('show');
    }

    $('.select2').select2({
        placeholder: "Selecione um item",
        dropdownParent: $('#addContentModal')
    });

    $(document).ready(function() {
        $('#editModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var menu_id = button.data('menu_id'); // Extract info from data-* attributes
            var modal = $(this);
            modal.find('.modal-body form').attr('action', '/admin/menus/' + menu_id);
            modal.find('.modal-body form input[name="_method"]').val('PUT');
            modal.find('#editModalLabel').text(`A Editar - ${button.data('menu_name')}`);

            $.ajax({
                url: '/{{ app()->getLocale() }}/backoffice/menus/' + menu_id + '/edit',
                type: 'GET',
                success: function(response){
                    modal.find('.modal-body').html(response.content);
                }
            });
        });

        $('.datatable').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtrado de _MAX_ registros no total)",
                "search": "Pesquisar",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Próximo"
                }
            },
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                { "orderable": false, "targets": 2 }
            ]
        });
    });
    </script>
@stop