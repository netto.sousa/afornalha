@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Comidas</h1>
@stop

@section('plugins.Datatables', true)

@section('content')
    <div class="main">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Inserir nova Comida</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('comidas.store', app()->getLocale()) }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Português)</label>
                                            <input type="text" class="form-control" name="name_pt" placeholder="Nome do menu em Português">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome (Inglês)</label>
                                            <input type="text" class="form-control" name="name_en" placeholder="Nome do menu em Inglês">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Preço</label>
                                            <input type="text" class="form-control" name="price" placeholder="Preço">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Categoria</label>
                                            <select class="form-control" name="category_id">
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name_pt }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Descrição (Português)</label>
                                            <textarea class="form-control" name="description_pt" placeholder="Descrição do menu em Português"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Descrição (Inglês)</label>
                                            <textarea class="form-control" name="description_en" placeholder="Descrição do menu em Inglês"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info btn-fill pull-right">Criar</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listagem de Comidas criadas</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Categoria
                                        </th>
                                        <th>
                                            Descrição
                                        </th>
                                        <th>
                                            Actions
                                        </th>
                                    </thead>
                                    <tbody>
                                        @if($foods->count() > 0)
                                        @foreach($foods as $food)
                                        <tr>
                                            <td>
                                                {{$food->id}}
                                            </td>
                                            <td>
                                                {{$food->name_pt}}
                                            </td>
                                            <td>
                                                {{$food->category->name_pt}}
                                            </td>
                                            <td>
                                                {{$food->description_pt ?? 'Sem descrição'}}
                                            </td>
                                            <td class="text-primary">
                                                <a href="{{ route('comidas.edit', [app()->getLocale(), $food->id]) }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editFoodModal" data-comida_id="{{ $food->id }}" data-name_pt="{{ $food->name_pt }}" data-name_en="{{ $food->name_en }}" data-description_pt="{{ $food->description_pt }}" data-description_en="{{ $food->description_en }}" data-price="{{ $food->price }}" data-category_id="{{ $food->category_id }}">Editar</a>
                                                
                                                <form action="{{ route('comidas.destroy', ['language' => app()->getLocale(), 'comida' => $food->id]) }}" method="POST" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-fill btn-sm"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="5" class="text-center">No foods found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="editFoodModal" tabindex="-1" role="dialog" aria-labelledby="editFoodModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editFoodModalLabel">Editar Comida</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" method="PUT">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome (Português)</label>
                                    <input type="text" class="form-control" name="name_pt" placeholder="Nome do menu em Português">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome (Inglês)</label>
                                    <input type="text" class="form-control" name="name_en" placeholder="Nome do menu em Inglês">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Preço</label>
                                    <input type="text" class="form-control" name="price" placeholder="Preço">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Categoria</label>
                                    <select class="form-control" name="category_id">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name_pt }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Descrição (Português)</label>
                                    <textarea class="form-control" name="description_pt" placeholder="Descrição do menu em Português"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Descrição (Inglês)</label>
                                    <textarea class="form-control" name="description_en" placeholder="Descrição do menu em Inglês"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary save-row">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    <script> 
    $('.select2').select2({
        placeholder: "Selecione um item",
    });
    $(document).ready(function() {
        $('#editFoodModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var comida_id = button.data('comida_id'); // Extract info from data-* attributes
            var modal = $(this);
            modal.find('.modal-body form').attr('action', '/{{ app()->getLocale() }}/backoffice/comidas/' + comida_id);
            modal.find('.modal-body form input[name="_method"]').val('PUT');
            modal.find('#editFoodModalLabel').text(`A Editar - ${button.data('name_pt')}`);
            modal.find('.modal-body form input[name="name_pt"]').val(button.data('name_pt'));
            modal.find('.modal-body form input[name="name_en"]').val(button.data('name_en'));
            modal.find('.modal-body form input[name="price"]').val(button.data('price'));
            modal.find('.modal-body form select[name="category_id"]').val(button.data('category_id'));
            modal.find('.modal-body form textarea[name="description_pt"]').val(button.data('description_pt'));
            modal.find('.modal-body form textarea[name="description_en"]').val(button.data('description_en'));
        });

        $('.save-row').click(function(e) {
                e.preventDefault();
                var url = $('#editFoodModal').find('.modal-body form').attr('action');
                var method = $('#editFoodModal').find('.modal-body form input[name="_method"]').val();
                console.log(url, method);
                $.ajax({
                    url: url,
                    type: method,
                    data: $('#editFoodModal .modal-body form').serialize(),
                    success: function(result) {
                        $('#editFoodModal').modal('hide');
                        location.reload();
                    }
                });
            });

        $('.datatable').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtrado de _MAX_ registros no total)",
                "search": "Pesquisar",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Próximo"
                }
            },
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                { "orderable": false, "targets": 2 }
            ]
        });
    });
    </script>
@stop