<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <title>
            Restaurante A Fornalha Nazaré - Carnes maturadas, peixes, Vegan
        </title>
        <meta content="Os melhores grelhados de carnes maturadas e não maturadas, pratos de peixe e pratos vegan. Após ver as ondas gigantes, divirta-se e desfrute do nosso espaço. Esperamos por sí na Nazaré." name="description">
        <meta content="Geraldo Netto" name="author">
        <meta content="7 saias Nazaré, Carnes Maturadas Nazaré, Ondas Nazaré, Onde comer Nazaré, Restaurante Leiria, Restaurante Vegan Nazaré, Restaurante Vegetariano Nazaré, Restaurantes Nazaré, Restaurantes Nazaré Beira Mar" name="keywords">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/gif" sizes="16x16">
        <!-- ex nihilo || October - November 2021 -->
        <!-- style start -->
        <link href="{{ asset("css/plugins.css") }}" media="all" rel="stylesheet" type="text/css">
        <link href="{{ asset("css/style.css") }}" media="all" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
        <!-- style end -->
        <!-- google fonts start -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900%7CMontserrat:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" type=
            "text/css">
        <link rel = "canonical" href = "https://www.afornalha.pt/pt" /> 
        <meta name = "robôs" content = "index, follow">
        <!-- google fonts end -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-QJTXZ7G312"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-QJTXZ7G312');
        </script>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-528ZFXK');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-528ZFXK"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!-- preloader start -->
        <div class="preloader-bg"></div>
        <div id="preloader">
            <div id="preloader-status">
                <div class="preloader-position loader">
                    <span></span>
                </div>
            </div>
        </div>
        <!-- preloader end -->
        <!-- border top start -->
        <div class="border-top top-position"></div>
        <!-- border top end -->
        <!-- border left start -->
        <div class="border-left left-position"></div>
        <!-- border left end -->
        <!-- border right start -->
        <div class="border-right right-position"></div>
        <!-- border right end -->
        <!-- border bottom start -->
        <div class="border-bottom bottom-position"></div>
        <!-- border bottom end -->
        <!-- navigation start -->
        <nav class="navbar navbar-fixed-top navbar-bg-switch">
            <!-- container start -->
            <div class="container">
                <div class="navbar-header fadeIn-element">
                    <!-- logo start -->
                    <div class="logo">
                        <a class="navbar-brand logo" href="{{ url('/') }}">
                            <!-- logo light start -->
                            <img alt="Logo" class="logo-light" src="{{ asset('img/logo-light.png') }}" alt="Logo A Fornalha branco">
                            <!-- logo light end -->
                            <!-- logo dark start -->
                            <img alt="Logo" class="logo-dark" src="{{ asset("img/logo-light.png") }}" alt="Logo A Fornalha escuro">
                            <!-- logo dark end -->
                        </a>
                    </div>
                    <!-- logo end -->
                </div>
                <!-- main navigation start -->
                <div class="main-navigation fadeIn-element">
                    <div class="navbar-header">
                        <button aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle
                        navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <!-- menu start -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#home">{{  __('home')  }}</a>
                            </li>
                            <li>
                                <a href="#about">{{  __('about')  }}</a>
                            </li>
                            <li>
                                <a href="#menu">{{  __('menu')  }}</a>
                            </li>
                            <li>
                                <a href="#feedback">{{  __('feedback')  }}</a>
                            </li>
                            <!-- <li>
                                <a href="#blank">Blank</a>
                            </li> -->
                            <li>
                                <a href="#gallery">{{  __('gallery')  }}</a>
                            </li>
                            <li>
                                <a href="#contact">{{  __('contacts')  }}</a>
                            </li>
                            <li>
                            <a class="social-media" target="_BLANK" href="https://www.facebook.com/restauranteafornalha">
                                <i class="fab fa-facebook-f icon"></i>    </a>
                            </li>
                            <li>
                                <a class="social-media" target="_BLANK" href="https://www.instagram.com/afornalharestaurante/"><i class="fab fa-instagram icon"></i></a>
                            </li>
                            <li>
                            <a class="social-media" target="_BLANK" href="https://www.tripadvisor.pt/Restaurant_Review-g315902-d3250898-Reviews-Restaurante_a_Fornalha-Nazare_Leiria_District_Central_Portugal.html"><i class="fab fa-tripadvisor icon"></i></a>
                            </li>
                            <li>
                                <select onChange="top.location.href=this.options[this.selectedIndex].value;" class="selectpicker language-picker" data-width="fit">
                                    <a href="#en">
                                        <option value="{{ url('/en') }}" @if(\App::getLocale() == 'en') selected @endif  data-content='<span class="flag-icon flag-icon-gb"></span>'></option>
                                    </a>
                                    <a href="#pt">
                                      <option @if(\App::getLocale() == 'pt') selected @endif value="{{ url('/pt') }}" data-content='<span class="flag-icon flag-icon-pt"></span>'></option>
                                    </a>
                                </select>
                            </li>
                        </ul>
                        <!-- menu end -->
                    </div>
                </div>
                <!-- main navigation end -->
            </div>
            <!-- container end -->
        </nav>
        <!-- navigation end -->
        <!-- home start -->
        <div class="upper-page" id="home">
            <!-- dots start -->
            <div class="dots">
                <div class="the-dots"></div>
            </div>
            <div class="dots-reverse">
                <div class="the-dots"></div>
            </div>
            <!-- dots end -->
            <!-- hero bg start -->
            <div class="hero-fullscreen">
                <div class="hero-fullscreen-FIX">
                    <div class="hero-bg">
                        <!-- hero slider wrapper start -->
                        <div class="swiper-container-wrapper">
                            <!-- swiper container start -->
                            <div class="swiper-container">
                                <!-- swiper wrapper start -->
                                <div class="swiper-wrapper">
                                    <!-- swiper slider item start -->
                                    <div class="swiper-slide">
                                        <div class="swiper-slide-inner" data-swiper-parallax="50%">
                                            <!-- swiper slider item IMG start -->
                                            <div class="swiper-slide-inner-bg bg-img-3">
                                                <!-- HTML5 video URL start -->
                                                <video playsinline autoplay muted loop>
                                                    <source src="videos/background-video-03.mp4" alt="A Fornalha Video Apresentação">
                                                </video>
                                                <!-- HTML5 video URL end -->
                                            </div>
                                            <!-- swiper slider item IMG end -->
                                            <!-- overlay start -->
                                            <div class="overlay overlay-dark-75"></div>
                                            <!-- overlay end -->
                                            <!-- swiper slider item txt start -->
                                            <div class="swiper-slide-inner-txt">
                                                <!-- section subtitle start -->
                                                <a href="#">
                                                    <h2 class="hero-subheading hero-subheading-home fadeIn-element introduction">
                                                        <span>{{  __('backgroundSubTitle')  }}</span>
                                                    </h2>
                                                </a>
                                                <!-- section subtitle end -->
                                                <!-- divider start -->
                                                <div class="divider-m"></div>
                                                <!-- divider end -->
                                                <!-- section title start -->
                                                <h1 class="hero-heading hero-heading-home fadeIn-element introduction">
                                                    {{  __('backgroundTitle')  }} <span>Nazaré</span>
                                                </h1>
                                                <!-- section title end -->
                                            </div>
                                            <!-- swiper slider item txt end -->
                                        </div>
                                    </div>
                                    <!-- swiper slider item end -->
                                    <!-- swiper slider item start -->
                                    <div class="swiper-slide">
                                        <div class="swiper-slide-inner" data-swiper-parallax="50%">
                                            <!-- swiper slider item IMG start -->
                                            <div class="swiper-slide-inner-bg bg-img-1">
                                            </div>
                                            <!-- swiper slider item IMG end -->
                                            <!-- overlay start -->
                                            <div class="overlay overlay-dark-75"></div>
                                            <!-- overlay end -->
                                            <!-- swiper slider item txt start -->
                                            <div class="swiper-slide-inner-txt">
                                                <!-- section subtitle start -->
                                                <a href="#">
                                                    <h2 class="hero-subheading hero-subheading-home fadeIn-element introduction">
                                                        <span>{{ __('backgroundSubTitle') }}</span>
                                                    </h2>
                                                </a>
                                                <!-- section subtitle end -->
                                                <!-- divider start -->
                                                <div class="divider-m"></div>
                                                <!-- divider end -->
                                                <!-- section title start -->
                                                <h1 class="hero-heading hero-heading-home fadeIn-element introduction">
                                                    {{  __('backgroundTitle')  }} <span>Nazaré</span>
                                                </h1>
                                                <!-- section title end -->
                                            </div>
                                            <!-- swiper slider item txt end -->
                                        </div>
                                    </div>
                                    <!-- swiper slider item end -->
                                    <!-- swiper slider item start -->
                                    <div class="swiper-slide">
                                        <div class="swiper-slide-inner" data-swiper-parallax="50%">
                                            <!-- swiper slider item IMG start -->
                                            <div class="swiper-slide-inner-bg bg-img-2">
                                            </div>
                                            <!-- swiper slider item IMG end -->
                                            <!-- overlay start -->
                                            <div class="overlay overlay-dark-75"></div>
                                            <!-- overlay end -->
                                            <!-- swiper slider item txt start -->
                                            <div class="swiper-slide-inner-txt">
                                                <!-- section subtitle start -->
                                                <a href="#">
                                                    <h2 class="hero-subheading hero-subheading-home fadeIn-element introduction">
                                                        <span>{{  __('backgroundTitle')  }}</span>
                                                    </h2>
                                                </a>
                                                <!-- section subtitle end -->
                                                <!-- divider start -->
                                                <div class="divider-m"></div>
                                                <!-- divider end -->
                                                <!-- section title start -->
                                                <h1 class="hero-heading hero-heading-home fadeIn-element introduction">
                                                    {{  __('backgroundTitle')  }} <span>Nazaré</span>
                                                </h1>
                                                <!-- section title end -->
                                            </div>
                                            <!-- swiper slider item txt end -->
                                        </div>
                                    </div>
                                    <!-- swiper slider item end -->
                                    <!-- swiper slider item start -->
                                    <div class="swiper-slide">
                                        <div class="swiper-slide-inner" data-swiper-parallax="50%">
                                            <!-- swiper slider item IMG start -->
                                            <div class="swiper-slide-inner-bg bg-img-4">
                                            </div>
                                            <!-- swiper slider item IMG end -->
                                            <!-- overlay start -->
                                            <div class="overlay overlay-dark-75"></div>
                                            <!-- overlay end -->
                                            <!-- swiper slider item txt start -->
                                            <div class="swiper-slide-inner-txt">
                                                <!-- section subtitle start -->
                                                <a href="#">
                                                    <h2 class="hero-subheading hero-subheading-home fadeIn-element introduction">
                                                        <span>{{  __('backgroundTitle')  }}</span>
                                                    </h2>
                                                </a>
                                                <!-- section subtitle end -->
                                                <!-- divider start -->
                                                <div class="divider-m"></div>
                                                <!-- divider end -->
                                                <!-- section title start -->
                                                <h1 class="hero-heading hero-heading-home fadeIn-element introduction">
                                                    {{  __('backgroundTitle')  }} <span>Nazaré</span>
                                                </h1>
                                                <!-- section title end -->
                                            </div>
                                            <!-- swiper slider item txt end -->
                                        </div>
                                    </div>
                                    <!-- swiper slider item end -->
                                </div>
                                <!-- swiper wrapper end -->   
                            </div>
                            <!-- swiper container end -->
                        </div>
                        <!-- hero slider wrapper end -->
                        <!-- swiper slider controls start -->
                        <div class="hero-slider-bg-controls fadeIn-element introduction">
                            <div class="swiper-slide-controls slide-prev">
                                <div class="ion-ios-arrow-left"></div>

                            </div>
                            <div class="swiper-slide-controls slide-next">
                                <div class="ion-ios-arrow-right"></div>
                            </div>
                        </div>
                        <!-- swiper slider controls end -->
                        <!-- swiper slider pagination start -->
                        <div class="swiper-slide-pagination fadeIn-element introduction"></div>
                        <!-- swiper slider pagination end -->
                        <!-- swiper slider play-pause start -->
                        <div class="swiper-slide-controls-play-pause-wrapper swiper-slide-controls-play-pause slider-on-off fadeIn-element introduction">
                            <div class="slider-on-off-switch">
                                <i class="ion-ios-play"></i>                            
                            </div>
                            <!-- swiper slider progress start -->
                            <div class="slider-progress-bar">
                                <span>
                                    <svg class="circle-svg" height="50" width="50">
                                        <circle class="circle" cx="25" cy="25" fill="none" r="24" stroke="#e0e0e0" stroke-width="2"></circle>
                                    </svg>
                                </span>
                            </div>
                            <!-- swiper slider progress end -->
                        </div>
                        <!-- swiper slider play-pause end -->
                    </div>
                </div>
            </div>
            <!-- hero bg end -->
            <!-- center container start -->
            <!-- center container end -->
            <!-- waves start -->
            <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                <defs>
                    <path id="gentle-wave-1" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                </defs>
                <g class="parallax">
                    <use xlink:href="#gentle-wave-1" x="48" y="0" fill="rgba(21, 21, 21, 0.7" />
                    <use xlink:href="#gentle-wave-1" x="48" y="3" fill="rgba(21, 21, 21, 0.5)" />
                    <use xlink:href="#gentle-wave-1" x="48" y="5" fill="rgba(21, 21, 21, 0.3)" />
                    <use xlink:href="#gentle-wave-1" x="48" y="7" fill="#151515" />
                </g>
            </svg>
            <!-- waves end -->
        </div>
        <!-- home end -->
        <!-- about start -->
        <section id="about" class="section-all bg-light">
            <!-- divider start -->
            <div class="divider-xl"></div>
            <!-- divider end -->
            <!-- container start -->
            <div class="container">
                <!-- row start -->
                <div class="row">
                    <!-- about wall start -->
                    <div class="wall-images popup-photo-gallery">
                        <a href="{{ asset("img/wall/1-big") }}.jpg">
                            <img class="wall-photo-1" src="{{ asset("img/wall/1.jpg") }}" alt="A Fornalha antiga entrada">
                        </a>
                        <a href="{{ asset("img/wall/2-big") }}.jpg">
                            <img class="wall-photo-2" src="{{ asset("img/wall/2.jpg") }}" alt="A Fornalha antiga 1995">
                        </a>
                        <a href="{{ asset("img/wall/3-big") }}.jpg">
                            <img class="wall-photo-3" src="{{ asset("img/wall/3.jpg") }}" alt="A Fornalha antiga foto">
                        </a>
                        <a href="{{ asset("img/wall/4-big") }}.jpg">
                            <img class="wall-photo-4" src="{{ asset("img/wall/4.jpg") }}" alt="A Fornalha Antiga">
                        </a>
                        <a href="{{ asset("img/wall/5-big") }}.jpg">
                            <img class="wall-photo-5" src="{{ asset("img/wall/5.jpg") }}" alt="Antiga Fornalha">
                        </a>
                        <a href="{{ asset("img/wall/6-big") }}.jpg">
                            <img class="wall-photo-6" src="{{ asset("img/wall/6.jpg") }}" alt="Fornalha Antiga">
                        </a>
                    </div>
                    <!-- about wall end -->
                </div>
                <!-- row end -->
                <!-- row start -->
                <div class="row">
                    <!-- about wall start -->
                    <div class="wall-images-reverse popup-photo-gallery">
                        <a href="{{ asset("img/wall/7-big.jpg") }}">
                            <img alt="A Fornalha remodelada" class="wall-photo-1" src="{{ asset("img/wall/7.jpg") }}">
                        </a>
                        <a href="{{ asset("img/wall/8-big.jpg") }}">
                            <img alt="A Fornalha novo conceito" class="wall-photo-2" src="{{ asset("img/wall/8.jpg") }}">
                        </a>
                        <a href="{{ asset("img/wall/9-big.jpg") }}">
                            <img alt="Restaurante A Fornalha novo espaço" class="wall-photo-3" src="{{ asset("img/wall/9.jpg") }}">
                        </a>
                        <a href="{{ asset("img/wall/10-big.jpg") }}">
                            <img alt="Restaurante A Fornalha novo conceito" class="wall-photo-4" src="{{ asset("img/wall/10.jpg") }}">
                        </a>
                        <a href="{{ asset("img/wall/11-big.jpg") }}">
                            <img alt="Restaurante A Fornalha" class="wall-photo-5" src="{{ asset("img/wall/11.jpg") }}"> 
                        </a>
                        <a href="{{ asset("img/wall/12-big.jpg") }}">
                            <img alt="A Fornalha depois das obras" class="wall-photo-6" src="{{ asset("img/wall/12.jpg") }}">
                        </a>
                    </div>
                    <!-- about wall end -->
                </div>
                <!-- row end -->
                <!-- row start -->
                <div class="row">
                    <!-- col start -->
                    <div class="col-lg-12">
                        <!-- section subtitle start -->
                        <h2 class="hero-subheading hero-subheading-dark">
                            {{  __('aboutSubTitle')  }}
                        </h2>
                        <!-- section subtitle end -->
                        <!-- divider start -->
                        <div class="divider-m"></div>
                        <!-- divider end -->
                        <!-- section title start -->
                        <h2 class="hero-heading hero-heading-dark">
                            A <span>Fornalha</span> - {{  __('aboutTitle')  }}
                        </h2>
                        <!-- section title end -->
                    </div>
                    <!-- col end -->
                </div>
                <!-- row end -->
                <!-- divider start -->
                <div class="divider-l"></div>
                <!-- divider end -->
                <!-- row start -->
                <div class="row">
                    <!-- col start -->
                    <div class="col-lg-12">
                        <!-- quote start -->
                        <div class="testimonial">
                            <div class="inner">
                                <div class="quote">
                                    <blockquote class="quote-inner">
                                        {{  __('aboutText')  }}
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <!-- quote end -->
                    </div>
                    <!-- col end -->
                </div>
                <!-- row end -->
                <!-- divider start -->
                <div class="divider-l"></div>
                <!-- divider end -->
                <!-- row start -->
                <div class="row">
                    <!-- col start -->
                    <div class="col-md-6 col-all">
                        <!-- section subtitle start -->
                        <h4>
                            <span class="color-switch">{{  __('aboutBe')  }}</span>{{  __('aboutFore')  }}                          
                        </h4>
                        <!-- section subtitle end -->
                        <!-- divider start -->
                        <div class="divider-m"></div>
                        <!-- divider end -->
                        <!-- section txt start -->
                        <div class="txt">
                            <p>
                                {{  __('aboutFirstText')  }}
                            </p>
                        </div>
                        <!-- section txt end -->
                        <!-- divider start -->
                        <div class="divider-l visible-mobile-devices"></div>
                        <!-- divider end -->
                    </div>
                    <!-- col end -->
                    <!-- col start -->
                    <div class="col-md-6 col-all">
                        <!-- section subtitle start -->
                        <h4>
                            <span class="color-switch">{{  __('aboutTo')  }}</span>{{  __('aboutDay')  }}  
                        </h4>
                        <!-- section subtitle end -->
                        <!-- divider start -->
                        <div class="divider-m"></div>
                        <!-- divider end -->
                        <!-- section txt start -->
                        <div class="txt">
                            <p>
                                {{  __('aboutSecondText')  }}
                            </p>
                        </div>
                        <!-- section txt end -->
                    </div>
                    <!-- col end -->
                </div> 
            </div>
            <!-- container end -->
            <!-- divider start -->
            <!-- divider end -->
        </section>
        <!-- about end -->
        <!-- timeline IMG start -->
        <div id="menu" class="timeline-image">
            <!-- intro start -->
            <div class="intro-years">
                <!-- section subtitle start -->
                <h2>
                    {{  __('beingAround')  }}
                </h2>
                <!-- section subtitle end -->
                <!-- section title start -->
                <h3 class="facts-counter-number">
                    41
                </h3>
                <!-- section title end -->
                <!-- section subtitle start -->
                <h4>
                    {{  __('years')  }}
                </h4>
                <!-- section subtitle end -->
            </div>
            <!-- intro end -->
        </div>
        <!-- timeline IMG end -->
        <!-- know-how start -->
        <section id="menu-section" class="section-all bg-light">
            <!-- container start -->
            <div class="container-fluid">
                <!-- row start -->
                <div class="row">
                    <!-- col start -->
                    <div class="col-md-6 nopadding">
                        <!-- slick right start -->
                        <div class="slick-right-alternative">
                            <!-- know-how carousel item 1 img start -->
                            <div class="img-fullwidth-wrapper">
                                <div class="img-fullwidth img-fullwidth-all img-fullwidth-know-how-carousel-1"></div>
                            </div>
                            <!-- know-how carousel item 1 img end -->
                            <!-- know-how carousel item 2 img start -->
                            <div class="img-fullwidth-wrapper">
                                <div class="img-fullwidth img-fullwidth-all img-fullwidth-know-how-carousel-2"></div>
                            </div>
                            <!-- know-how carousel item 2 img end -->
                        </div>
                        <!-- slick right end -->
                        <!-- waves start -->
                        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                            <defs>
                                <path id="gentle-wave-2" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                            </defs>
                            <g class="parallax">
                                <use xlink:href="#gentle-wave-2" x="48" y="0" fill="rgba(21, 21, 21, 0.7" />
                                <use xlink:href="#gentle-wave-2" x="48" y="3" fill="rgba(21, 21, 21, 0.5)" />
                                <use xlink:href="#gentle-wave-2" x="48" y="5" fill="rgba(21, 21, 21, 0.3)" />
                                <use xlink:href="#gentle-wave-2" x="48" y="7" fill="#151515" />
                            </g>
                        </svg>
                        <!-- waves end -->
                    </div>
                    <!-- col end -->
                    <!-- col start -->
                    <div class="col-md-6 nopadding">
                        <!-- slick left start -->
                        <div class="slick-left">
                            <div class="blockquote">
                                <!-- center container start -->
                                <div class="center-container">
                                    <!-- center block start -->
                                    <div class="center-block">
                                        <h2 class="hero-heading hero-heading-dark">
                                            {{  __('our3')  }} <span>{{  __('menu')  }}</span>
                                        </h2>
                                        <div class="txt" style="margin:0px 30px;">
                                            <p>
                                                {{  __('ourMenuText')  }}
                                            </p>
                                        </div>
                                        <div class="more-wraper-center">
                                            <a href="{{  __('linkMenu')  }}">
                                                <div class="pulse more-button-bg-center more-button-circle"></div>
                                                <div class="more-button-txt-center">
                                                    <span>{{  __('viewMenu')  }}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- center block end -->
                                </div>
                                <!-- center container end -->
                            </div>
                        </div>
                        <!-- slick left end -->
                    </div>
                    <!-- col end -->
                </div>
                <!-- row end -->
            </div>
            <!-- container end -->
        </section>
        <!-- know-how end -->
        <!-- feedback start -->
        <section id="feedback" class="section-all bg-dark">
            <!-- divider start -->
            <div class="divider-xl"></div>
            <!-- divider end -->
            <!-- container start -->
            <div class="container">
                <!-- row start -->
                <div class="row">
                    <!-- col start -->
                    <div class="col-lg-12">
                        <!-- section subtitle start -->
                        <h2 class="hero-subheading hero-subheading-dark">
                            {{  __('yourFeedback')  }}
                        </h2>
                        <!-- section subtitle end -->
                        <!-- divider start -->
                        <div class="divider-m"></div>
                        <!-- divider end -->
                        <!-- section title start -->
                        <h2 class="hero-heading hero-heading-dark">
                            {{  __('our3')  }} <span>{{  __('service')  }}</span>
                        </h2>
                        <!-- section title end -->
                    </div>
                    <!-- col end -->
                </div>
                <!-- row end -->
                <!-- divider start -->
                <div class="divider-l"></div>
                <!-- divider end -->
                <!-- row start -->
                <div class="row">
                    <div class="services-steps-wrapper">
                        <!-- col start -->
                        <div class="col-md-4 col-all services-block">
                            <!-- section subtitle start -->
                            <h4 class="service-heading">
                                <span class="color-switch">{{  __('feedbackFirstTitle1')  }}</span> {{  __('feedbackFirstTitle2')  }}
                            </h4>
                            <!-- section subtitle end -->
                            <!-- divider start -->
                            <div class="divider-m"></div>
                            <!-- divider end -->
                            <!-- section txt start -->
                            <!-- section subtitle start -->
                            <div class="service-number">
                                1
                            </div>
                            <!-- section subtitle end -->
                            <!-- section txt start -->
                            <p>
                                {{  __('feedbackFirstText')  }}
                            </p>
                            <!-- section txt end -->
                        </div>
                        <!-- col end -->
                        <!-- divider start -->
                        <div class="divider-l visible-mobile-devices"></div>
                        <!-- divider end -->
                        <!-- col start -->
                        <div class="col-md-4 col-all services-block pull-up">
                            <!-- section subtitle start -->
                            <h4 class="service-heading">
                                <span class="color-switch">{{  __('feedbackSecondTitle1')  }}</span> {{  __('feedbackSecondTitle2')  }}
                            </h4>
                            <!-- section subtitle end -->
                            <!-- divider start -->
                            <div class="divider-m"></div>
                            <!-- divider end -->
                            <!-- section subtitle start -->
                            <div class="service-number">
                                2
                            </div>
                            <!-- section subtitle end -->
                            <!-- section txt start -->
                            <p>
                                {{  __('feedbackSecondText')  }}                           
                            </p>
                            <!-- section txt end -->
                        </div>
                        <!-- col end -->
                        <!-- divider start -->
                        <div class="divider-l visible-mobile-devices"></div>
                        <!-- divider end -->
                        <!-- col start -->
                        <div class="col-md-4 col-all services-block pull-up">
                            <!-- section subtitle start -->
                            <h4 class="service-heading">
                                <span class="color-switch">{{  __('feedbackThirdTitle1')  }}</span> {{  __('feedbackThirdTitle2')  }}
                            </h4>
                            <!-- section subtitle end -->
                            <!-- divider start -->
                            <div class="divider-m"></div>
                            <!-- divider end -->
                            <!-- section subtitle start -->
                            <div class="service-number">
                                3
                            </div>
                            <!-- section subtitle end -->
                            <!-- section txt start -->
                            <p>
                                {{  __('feedbackThirdText')  }}                         
                            </p>
                            <!-- section txt end -->
                        </div>
                        <!-- col end -->
                        <!-- Tripadvisor-->
                        <div class="col-md-12 col-all services-block pull-up">
                            <div class="tripadvisor-section">
                                <div id="TA_cdswritereviewlg540" class="TA_cdswritereviewlg"><ul id="BnsoOCPVOZK" class="TA_links DQ2xltqY"><li id="xzlgkoGWQL" class="DwfarJGRrj"><a target="_blank" href="https://www.tripadvisor.com/Restaurant_Review-g315902-d3250898-Reviews-Restaurante_a_Fornalha-Nazare_Leiria_District_Central_Portugal.html"><img src="https://static.tacdn.com/img2/brand_refresh/Tripadvisor_lockup_horizontal_secondary_registered.svg" alt="TripAdvisor"/></a></li></ul></div><script async src="https://www.jscache.com/wejs?wtype=cdswritereviewlg&amp;uniq=540&amp;locationId=3250898&amp;lang=en_UK&amp;lang=en_UK&amp;display_version=2" data-loadtrk onload="this.loadtrk=true"></script>
                            </div>
                        </div>
                        <!-- Tripadvisor end-->
                    </div>
                </div>
                <!-- row end -->
            </div>
            <!-- container end -->
            <!-- divider start -->
            <div class="divider-xl"></div>
            <!-- divider end -->
        </section>
        <!-- services end -->
        <!-- services 2 start -->
        <section id="gallery" class="section-all bg-light">
            <!-- divider start -->
            <div class="divider-xl"></div>
            <!-- divider end -->
            <!-- row start -->
            <div class="row">
                <!-- col start -->
                <div class="col-lg-12">
                    <!-- section subtitle start -->
                    <h2 class="hero-subheading hero-subheading-dark">
                        {{  __('somePictures')  }}
                    </h2>
                    <!-- section subtitle end -->
                    <!-- divider start -->
                    <div class="divider-m"></div>
                    <!-- divider end -->
                    <!-- section title start -->
                    <h2 class="hero-heading hero-heading-dark">
                        {{  __('our2')  }} <span>{{  __('gallery')  }}</span>
                    </h2>
                    <!-- section title end -->
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
            <!-- container start -->
            <div class="container-fluid">
                <!-- row start -->
                <div class="row gallery popup-photo-gallery">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/1-big") }}.jpg">
                            <img alt="Restaurante A Fornalha depois das obras" class="img-responsive" src="{{ asset("img/gallery/1.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/2-big") }}.jpg">
                            <img alt="Teste de stress restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/2.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/3-big") }}.jpg">
                            <img alt="Salão dois do Restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/3.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/4-big") }}.jpg">
                            <img alt="Prato de entrada Restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/4.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/5-big") }}.jpg">
                            <img alt="Salão um Restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/5.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/6-big") }}.jpg">
                            <img alt="Sobremesa restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/6.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/7-big") }}.jpg">
                            <img alt="Restaurante A fornalha salão 1" class="img-responsive" src="{{ asset("img/gallery/7.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/8-big") }}.jpg">
                            <img alt="Salão um A Fornalha" class="img-responsive" src="{{ asset("img/gallery/8.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/9-big") }}.jpg">
                            <img alt="Risoto de cogumelos, restaurante a fornalha" class="img-responsive" src="{{ asset("img/gallery/9.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/10-big") }}.jpg">
                            <img alt="Nova entrada Restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/10.jpg") }}">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ asset("img/gallery/11-big") }}.jpg">
                            <img alt="Hamburguer vegan restaurante A Fornalha" class="img-responsive" src="{{ asset("img/gallery/11.jpg") }}">
                        </a>
                    </div>
                </div>
                <!-- row end -->
            </div>
            <!-- container end -->
            <!-- divider start -->
            <div class="divider-m"></div>
            <!-- divider end -->
        </section>
        <!-- services 2 end -->
        <!-- blank start -->
        <section id="blank" class="section-all">
            <!-- container start -->
            <div class="container-fluid">
                <!-- row start -->
                <div class="row">
                    <!-- parallax wrapper start -->
                    <div class="parallax parallax-all parallax-blank" data-parallax-speed="0.75">
                        <!-- parallax borders start -->
                        <div class="borders"></div>
                        <!-- parallax borders end -->
                        <!-- parallax overlay start -->
                        <div class="parallax-overlay"></div>
                        <!-- parallax overlay end -->
                        <!-- parallax content start -->
                        <div class="parallax-content">
                            <!-- button start -->
                            <div class="more-wraper-center">
                                <a data-lity="" href="https://www.youtube.com/watch?v=hhW3LnECqgg">
                                    <div class="pulse more-button-bg-center more-button-circle"></div>
                                    <div class="more-button-txt-center">
                                        <span>{{  __('watchVideo')  }}</span>
                                    </div>
                                </a>
                            </div>
                            <!-- button end -->
                        </div>
                        <!-- parallax content end -->
                        <!-- waves start -->
                        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                            <defs>
                                <path id="gentle-wave-4" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                            </defs>
                            <g class="parallax">
                                <use xlink:href="#gentle-wave-4" x="48" y="0" fill="rgba(21, 21, 21, 0.7" />
                                <use xlink:href="#gentle-wave-4" x="48" y="3" fill="rgba(21, 21, 21, 0.5)" />
                                <use xlink:href="#gentle-wave-4" x="48" y="5" fill="rgba(21, 21, 21, 0.3)" />
                                <use xlink:href="#gentle-wave-4" x="48" y="7" fill="#151515" />
                            </g>
                        </svg>
                        <!-- waves end -->
                    </div>
                    <!-- parallax wrapper end -->
                </div>
                <!-- row end -->
            </div>
            <!-- container end -->
        </section>
        <!-- blank end -->
        <!-- contact start -->
        <section id="contact" class="section-all">
            <!-- container start -->
            <div class="container-fluid">
                <!-- row start -->
                <div class="row">
                    <!-- parallax wrapper start -->
                    <div class="parallax parallax-all parallax-contact" data-parallax-speed="0.75">
                        <!-- parallax borders start -->
                        <div class="borders"></div>
                        <!-- parallax borders end -->
                        <!-- parallax overlay start -->
                        <div class="parallax-overlay"></div>
                        <!-- parallax overlay end -->
                        <!-- parallax content start -->
                        <div class="parallax-content">
                            <!-- section subtitle start -->
                            <h2 class="hero-subheading">
                                {{  __('needReservation')  }} 
                            </h2>
                            <!-- section subtitle end -->
                            <!-- divider start -->
                            <div class="divider-m"></div>
                            <!-- divider end -->
                            <!-- section title start -->
                            <h2 class="hero-heading">
                                {{  __('bookYour')  }}<span>{{  __('table')  }}</span>
                            </h2>
                            <!-- section title end -->
                            <!-- divider start -->
                            <div class="divider-l"></div>
                            <!-- divider end -->
                            <!-- contact info start -->
                            <div class="contact-info-wrapper">
                                <!-- col start -->
                                <div class="col-md-4 col-sm-12">
                                    <div class="contact-info-description">
                                        <i class="ion-ios-location-outline contact-info-description-img"></i> <span class="contact-info-text">Rua Dr. António Duarte Pimpão 12,<br> Nazaré 2450-170,<br> Portugal</span>
                                    </div>
                                    <!-- divider start -->
                                    <div class="divider-m visible-mobile-devices"></div>
                                    <!-- divider end -->
                                </div>
                                <!-- col end -->
                                <!-- col start -->
                                <div class="col-md-4 col-sm-12">
                                    <div class="contact-info-description">
                                        <i class="ion-ios-telephone-outline contact-info-description-img large"></i> <span class="contact-info-text large">+351 933 711 526 </br>
                                        <h4 class="text-center contact-call-message">
                                        ( {{  __('callmessage')  }} )</h4></span>
                                    </div>
                                    <!-- divider start -->
                                    <div class="divider-m visible-mobile-devices"></div>
                                    <!-- divider end -->
                                </div>
                                <!-- col end -->
                                <!-- col start -->
                                <div class="col-md-4 col-sm-12">
                                    <div class="contact-info-description">
                                        <i class="ion-ios-email-outline contact-info-description-img"></i> <span class="contact-info-text"><a class="link-effect link-effect-light" href="mailto:geral@afornalha.pt">geral@afornalha.pt</a></span>
                                    </div>
                                </div>
                                <!-- col end -->
                                <!-- col start -->
                                <div class="col-md-12 col-sm-12">
                                    <!-- divider start -->
                                    <div class="divider-l"></div>
                                    <!-- divider end -->
                                    <div class="contact-info-description">
                                        <!-- button start -->
                                        <div class="more-wraper-center contact-modal-launcher">
                                            <div class="pulse more-button-bg-center more-button-circle"></div>
                                            <div class="more-button-txt-center">
                                                <span>{{  __('contactUs')  }}</span>
                                            </div>
                                        </div>
                                        <!-- button end -->
                                    </div>
                                </div>
                                <!-- col end -->
                            </div>
                            <!-- contact info end -->
                        </div>
                        <!-- parallax content end -->
                        <!-- waves start -->
                        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                            <defs>
                                <path id="gentle-wave-5" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                            </defs>
                            <g class="parallax">
                                <use xlink:href="#gentle-wave-5" x="48" y="0" fill="rgba(21, 21, 21, 0.7" />
                                <use xlink:href="#gentle-wave-5" x="48" y="3" fill="rgba(21, 21, 21, 0.5)" />
                                <use xlink:href="#gentle-wave-5" x="48" y="5" fill="rgba(21, 21, 21, 0.3)" />
                                <use xlink:href="#gentle-wave-5" x="48" y="7" fill="#151515" />
                            </g>
                        </svg>
                        <!-- waves end -->
                    </div>
                    <!-- parallax wrapper end -->
                </div>
                <!-- row end -->
            </div>
            <!-- container end -->
        </section>
        <!-- contact end -->
        <!-- contact modal start -->
        <div class="contact-modal">
            <!-- container start -->
            <div class="container">
                <!-- center container start -->
                <div class="center-container-contact-modal">
                    <!-- center block start -->
                    <div class="center-block-contact-modal">
                        <!-- row start -->
                        <div class="row center-block-contact-modal-padding-top">
                            <div class="col-lg-12">
                                <!-- section subtitle start -->
                                <h2 class="hero-subheading">
                                    {{  __('formTitle')  }}
                                </h2>
                                <!-- section subtitle end -->
                                <!-- divider start -->
                                <div class="divider-m"></div>
                                <!-- divider end -->
                                <!-- section title start -->
                                <h2 class="hero-heading">
                                    {{  __('please')  }} <span>{{  __('write')  }}</span>
                                </h2>
                                <!-- section title end -->
                                <!-- divider start -->
                                <div class="divider-l"></div>
                                <!-- divider end -->
                            </div>
                        </div>
                        <!-- row end -->
                        <!-- row start -->
                        <div class="row contact-modal-wrapper">
                            <!-- col start -->
                            <div>
                                <!-- contact form start -->
                                <div id="contact-form">
                                    <form action="{{ url("sendform") }}" id="form" method="post" name="send">
                                        {{ csrf_field() }}
                                        <!-- col start -->
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <input class="requiredField name" id="name" name="name" placeholder="{{  __('name')  }}" type="text">
                                        </div>
                                        <!-- col end -->
                                        <!-- col start -->
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <input class="requiredField email" id="email" name="email" placeholder="{{  __('email')  }}" type="text">
                                        </div>
                                        <!-- col end -->
                                        <!-- col start -->
                                        <div class="make-space">
                                            <input class="requiredField subject" id="subject" name="subject" placeholder="{{  __('subject')  }}" type="text">
                                        </div>
                                        <!-- col end -->
                                        <!-- col start -->
                                        <div class="make-space">
                                            <textarea class="requiredField message" id="message" name="message" placeholder="{{  __('message')  }}"></textarea>
                                        </div>
                                        <!-- col end -->
                                        <div>
                                            <!-- button start -->
                                            <div class="more-wraper-center more-wraper-center-form">
                                                <div class="more-button-bg-center more-button-circle"></div>
                                                <div class="more-button-txt-center">
                                                    <button>
                                                    <span>{{  __('submit')  }}</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- button end -->
                                        </div>
                                    </form>
                                </div>
                                <!-- contact form end -->
                            </div>
                            <!-- col end -->
                        </div>
                        <!-- row end -->
                        <!-- divider start -->
                        <div class="divider-l"></div>
                        <!-- divider end -->
                        <!-- row start -->
                        <div class="row center-block-contact-modal-padding-bottom">
                            <div class="col-lg-12">
                                <!-- contact modal closer start -->
                                <div class="contact-modal-closer">
                                    <span class="ion-close"></span>
                                </div>
                                <!-- contact modal closer end -->
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- center block end -->
                </div>
                <!-- center container end -->
            </div>
            <!-- container end -->
        </div>
        <!-- contact modal end -->
        <!-- footer start -->
        <section id="footer" class="section-all bg-light">
            <!-- container start -->
            <div class="container-fluid">
                <!-- row start -->
                <div class="row">
                    <!-- col start -->
                    <div class="col-lg-12">
                        <!-- section subtitle start -->
                        <h2 class="hero-subheading hero-subheading-dark">
                            {{  __('whereToFindUs')  }}
                        </h2>
                        <!-- section subtitle end -->
                        <!-- divider start -->
                        <div class="divider-m"></div>
                        <!-- divider end -->
                        <!-- section title start -->
                        <h2 class="hero-heading hero-heading-dark">
                            {{  __('the')  }} <span>{{  __('location')  }}</span>
                        </h2>
                        <!-- section title end -->
                    </div>
                    <!-- col end -->
                </div>
                <!-- row end -->
                <!-- divider start -->
                <div class="divider-l"></div>
                <!-- divider end -->
                <!-- row start -->
                <div class="row">
                    <!-- google maps wrapper start -->
                    <div id="google-maps-wrapper">
                        <!-- google maps start -->
                        <div class="google-maps">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1536.9860463607174!2d-9.068171230931227!3d39.60531111548056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd18a9bb3ec7f819%3A0xe39d65f8d74ab8bb!2sRestaurante%20A%20FORNALHA%20-%20Nazar%C3%A9!5e0!3m2!1spt-PT!2spt!4v1638834445043!5m2!1spt-PT!2spt"></iframe>
                        </div>
                        <!-- google maps end -->
                    </div>
                    <!-- google maps wrapper end -->
                </div>
                <!-- row end -->
                <!-- divider start -->
                <div class="divider-l"></div>
                <!-- divider end -->
                <!-- row start -->
                <div class="container row footer-credits center-container" style="position: static!important;">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <h2 class="hero-subheading hero-subheading-dark">
                            {{  __('navigationMenu')  }}
                        </h2>
                        <ul class="links">
                            <li><a href="#home">{{  __('home')  }}</a></li>
                            <li><a href="#about">{{  __('about')  }}</a></li>
                            <li><a href="#menu">{{  __('menu')  }}</a></li>
                            <li><a href="#feedback">{{  __('feedback')  }}</a></li>
                            <li><a href="#gallery">{{  __('gallery')  }}</a></li>
                            <li><a href="#contacts">{{  __('contacts')  }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4col-sm-6">
                        <!-- footer logo start -->
                        <div class="footer-credits-logo">
                             
                            <a target="_BLANK" href="https://www.livroreclamacoes.pt/Pedido/Reclamacao"><img width="150px" alt="Livro de Reclamações" src="img/livro-de-reclamacoes.png"></a><br>

                            <a target="_BLANK" href="https://covid19.min-saude.pt/"><img width="120px" alt="Clean and Safe Covid" src="img/clean-and-safe.png"></a>
                            <br>
                            <a target="_BLANK" href="img/projeto.pdf"><img style="padding:5px; background-color:#fff;" height="50px" src="img/barra_cofin_FEDER.jpg"></a>
                            
                        </div>
                        <!-- footer logo end -->
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <h2 class="hero-subheading hero-subheading-dark">
                            {{  __('openingHours')  }}
                        </h2>
                        <ul class="opening-hours">
                            <li><strong>{{  __('hoursFirstTitle')  }}</strong><br>
                            18:00h – 23:00h</li>
                            <li>
                            <li><strong>{{  __('hoursSaturday')  }}</strong><br>
                            12:00h – 15:00h <br>
                            19:00h – 00:00h
                            </li>
                        </ul>
                    </div>
                    <!-- col start -->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <!-- copyright start -->
                        <!-- divider start -->
                        <div class="divider-l"></div>
                        <!-- divider end -->
                        <div class="copyright">
                            &copy; All Rights Reserved | Luciano&Filhos Lda
                        </div>
                        <!-- copyright end -->
                    </div>
                    <!-- col end -->
                </div>
                <!-- row end -->
            </div>
            <!-- container end -->
            <!-- divider start -->
            <div class="divider-m"></div>
            <!-- divider end -->
        </section>
        <!-- footer end -->
        <!-- to top arrow start -->
        <a href="#home">
            <div class="to-top-arrow">
                <span class="ion-ios-arrow-up"></span>
            </div>
        </a>
        <!-- to top arrow end -->
        <!-- photoSwipe background start -->
        <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                            "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                            "Zoom in/out"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
                        "Next (arrow right)"></button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>

        <a href="https://wa.me/351933711526?text=Olá!" class="whatsapp-float-icon" target="_blank">
        <i class="fab fa-whatsapp"></i>
        </a>

        <select onChange="top.location.href=this.options[this.selectedIndex].value;" class="selectpicker language-picker-mobile" data-width="fit">
            <a href="#en">
                <option value="{{ url('/en') }}" @if(\App::getLocale() == 'en') selected @endif  data-content='<span class="flag-icon flag-icon-gb"></span>'></option>
            </a>
            <a href="#pt">
              <option @if(\App::getLocale() == 'pt') selected @endif value="{{ url('/pt') }}" data-content='<span class="flag-icon flag-icon-pt"></span>'></option>
            </a>
        </select>

        <!-- photoSwipe background end -->
        <!-- scripts start -->
        <script src="{{ asset("js/plugins.js") }}"></script> 
        <script src="{{ asset("js/foodex-01.js") }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
        <!-- scripts end -->
    </body>
</html>