<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
            'id' => 1,
            'name' => 'Netto Sousa',
            'email' => 'netto.sousa@gmail.com',
            'password' => bcrypt('307860qw'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        ];

        DB::table('users')->insert($users);
    }
}
