<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $categories = [
                [
                'id' => 1,
                'name_pt' => 'Pratos de Peixe',
                'name_en' => 'Fish Dishes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ], 
                [
                'id' => 2,
                'name_pt' => 'Pratos de Carne',
                'name_en' => 'Meat Dishes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ], 
                [
                'id' => 3,
                'name_pt' => 'Pratos Vegan',
                'name_en' => 'Vegan Dishes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ], 
                [
                'id' => 4,
                'name_pt' => 'Sobremesas',
                'name_en' => 'Desserts',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            ];
            DB::table('categories')->insert($categories);
        }
    }
}
