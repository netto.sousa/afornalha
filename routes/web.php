<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::redirect('/', '/pt');

Route::group(['prefix' => '{language}'], function(){
    Route::get('/', function () {
        return view('index');
    });
    Route::resource('/backoffice/categorias', 'CategoryController')->middleware('auth');
    Route::resource('/backoffice/comidas', 'FoodController')->middleware('auth');
    
    Route::resource('/backoffice/menus', 'MenuController')->middleware('auth');
    Route::get('/backoffice', 'MenuController@index')->middleware('auth');
    Route::delete('/backoffice/menusContent/{id}', 'MenuController@destroy')->middleware('auth');
    Route::delete('/backoffice/menusContent/{id}', ['as' => 'menusContent.destroy', 'uses' => 'MenuController@menuContentDestroy'])->middleware('auth');
});

Route::post('sendform', 'MailTreatController@sendContactForm');
Auth::routes();

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');
